using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarea_6.Models;

namespace Tarea_6.ViewModels;
public partial class MainViewModel:ObservableObject
{
    private Stack<string> mensajesDeError = new Stack<string>();
    private List<User> usuarios = new List<User>
    {
        new User { Nombre = "julian", Apellido="sandoval", CorreoElectronico="julianSandoval@gmail.com" },
        new User { Nombre = "Eudal", Apellido="Fuentes", CorreoElectronico="eudalfuentes@gmail.com" },
        new User { Nombre = "Carlos", Apellido="Rojas", CorreoElectronico="carlosrojas@gmail.com" },
        new User { Nombre = "Fernando", Apellido="Hinojosa", CorreoElectronico="fernandohinojosa@gmail.com" },
        new User { Nombre = "Rossy", Apellido="Torrez", CorreoElectronico="rossytorrez@gmail.com" }
    };

    [ObservableProperty]
    private IEnumerable<User> usuariosLinq;
    [ObservableProperty]
    private string? tNombre;
    [ObservableProperty]
    private string? tApellido;
    [ObservableProperty]
    private string? tCorreoElectronico;
    [ObservableProperty]
    private string? bCorreoElectronico;

    [RelayCommand]
    private void InitQuery()
    {
        UsuariosLinq = from usuario in usuarios
                       select usuario;
    }

    [RelayCommand]
    private void QueryAgregarUsuario()
    {

        try
        {
            var newUser = new User
            {
                Nombre = tNombre,
                Apellido = tApellido,
                CorreoElectronico = tCorreoElectronico
            };
            usuarios.Add(newUser);
            InitQuery();
        }
        catch (Exception ex)
        {
            mensajesDeError.Push($"{DateTime.Now}: {ex.Message}");
        }
    }

    [RelayCommand]
    private void OrdenarAscendente()
    {
        UsuariosLinq = UsuariosLinq.OrderBy(user => user.Nombre);
    }
    [RelayCommand]
    private void OrdenarDescendente()
    {
        UsuariosLinq = UsuariosLinq.OrderByDescending(user => user.Nombre);
    }
    [RelayCommand]
    private void BuscarUsuario()
    {
        UsuariosLinq = usuarios.Where(user => user.CorreoElectronico.Contains(bCorreoElectronico));
    }
}
