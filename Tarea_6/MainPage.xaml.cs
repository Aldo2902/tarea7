using Tarea_6.ViewModels;

namespace Tarea_6;

public sealed partial class MainPage : Page
{
    private MainViewModel vm;
    public MainPage()
    {
        this.InitializeComponent();
        this.vm = (MainViewModel)this.DataContext;
    }
    private void Page_Loaded(object sender, RoutedEventArgs e)
    {
        this.vm.InitQueryCommand.Execute(null);
    }
}
